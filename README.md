# Test Creator

Project that allows the creation of Tests for Students courses. 
Administrators and Instructors will be able to manage Students, Questions, Tests and Courses, while Student will be able to answer Tests and be graded.
The project has included Unit Tests for learning purposes of JUnit and code coverage.

Technologies used:
DB: MySQL 5
Language: Java 11
Libraries: JUnit 5 (Jupiter)
