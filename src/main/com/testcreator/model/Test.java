package com.testcreator.model;

import java.util.*;

public class Test extends BaseModel {

    public final static String IDINSTRUCTOR = "idinstructor";

    private Integer idInstructor;

    public Test() {}

    public Test(Integer id, String name, Integer idInstructor) {
        setId(id);
        setName(name);
        setIdInstructor(idInstructor);
    }

    public Test(String name, Integer idInstructor) {
        setName(name);
        setIdInstructor(idInstructor);
    }

    public Integer getIdInstructor() {
        return idInstructor;
    }

    public void setIdInstructor(Integer idInstructor) {
        this.idInstructor = idInstructor;
    }
}
