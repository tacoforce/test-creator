package com.testcreator.model;

public class Student extends BaseModel {

    public static final String LASTNAME = "lastname";

    private String lastname;

    public Student() {}

    public Student(Integer id, String name, String lastname) {
        setId(id);
        setName(name);
        setLastname(lastname);
    }

    public Student(String name, String lastname) {
        setName(name);
        setLastname(lastname);
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
