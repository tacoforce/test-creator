package com.testcreator.model;

import java.util.Date;
import java.util.UUID;

public abstract class BaseModel {

    public final static String ID = "id";
    public final static String NAME = "name";

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
