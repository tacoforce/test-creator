package com.testcreator.model;

public class StudentTest {

    public final static String ID = "id";
    public final static String IDSTUDENT = "idstudent";
    public final static String IDTEST = "idtest";
    public final static String RESULT = "result";

    private Integer id;
    private Integer idStudent;
    private Integer idTest;
    private Integer result;

    public StudentTest() {}

    public StudentTest(Integer idStudent, Integer idTest, Integer result) {
        setIdStudent(idStudent);
        setIdTest(idTest);
        setResult(result);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public Integer getIdTest() {
        return idTest;
    }

    public void setIdTest(Integer idTest) {
        this.idTest = idTest;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
