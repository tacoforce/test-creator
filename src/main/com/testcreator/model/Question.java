package com.testcreator.model;

import java.util.UUID;

public class Question {

    public final static String ID = "id";
    public final static String QUESTION = "question";
    public final static String ANSWER = "answer";
    public final static String IDINSTRUCTOR = "idinstructor";

    private Integer id;
    private String question;
    private Boolean answer;
    private Integer idInstructor;

    public Question() {}

    public Question(String question, boolean answer, Integer idInstructor) {
        setQuestion(question);
        setAnswer(answer);
        setIdInstructor(idInstructor);
    }

    public Question(int id, String question, boolean answer, Integer idInstructor) {
        setId(id);
        setQuestion(question);
        setAnswer(answer);
        setIdInstructor(idInstructor);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    public Integer getIdInstructor() {
        return idInstructor;
    }

    public void setIdInstructor(Integer idInstructor) {
        this.idInstructor = idInstructor;
    }
}
