package com.testcreator.model;

import java.util.UUID;

public class Instructor extends BaseModel {

    public static final String LASTNAME = "lastname";

    private String lastname;

    public Instructor() {}

    public Instructor(String name, String lastname) {
        setName(name);
        setLastname(lastname);
    }

    public Instructor(Integer id, String name, String lastname) {
        setId(id);
        setName(name);
        setLastname(lastname);
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
