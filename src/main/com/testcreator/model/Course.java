package com.testcreator.model;

public class Course extends BaseModel {

    public final static String IDINSTRUCTOR = "idinstructor";

    private Integer idInstructor;

    public Course() {}

    public Course(String name, Integer idInstructor) {
        setName(name);
        setIdInstructor(idInstructor);
    }

    public Course(int id, String name, Integer idInstructor) {
        setId(id);
        setName(name);
        setIdInstructor(idInstructor);
    }

    public Integer getIdInstructor() {
        return idInstructor;
    }

    public void setIdInstructor(Integer idInstructor) {
        this.idInstructor = idInstructor;
    }
}
