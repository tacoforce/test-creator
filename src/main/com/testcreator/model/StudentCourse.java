package com.testcreator.model;

public class StudentCourse {

    public final static String ID = "id";
    public final static String IDSTUDENT = "idstudent";
    public final static String IDCOURSE = "idcourse";

    private Integer id;
    private Integer idStudent;
    private Integer idCourse;

    public StudentCourse(Integer idStudent, Integer idCourse) {
        setIdStudent(idStudent);
        setIdCourse(idCourse);
    }

    public StudentCourse(int id, Integer idStudent, Integer idCourse) {
        setId(id);
        setIdStudent(idStudent);
        setIdCourse(idCourse);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public Integer getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Integer idCourse) {
        this.idCourse = idCourse;
    }
}
