package com.testcreator.model;

public class QuestionTest {

    public final static String ID = "id";
    public final static String IDTEST  = "idtest";
    public final static String IDQUESTION = "idquestion";

    private Integer id;
    private Integer idTest;
    private Integer idQuestion;

    public QuestionTest() {}

    public QuestionTest(Integer idQuestion, Integer idTest) {
        setIdQuestion(idQuestion);
        setIdTest(idTest);
    }

    public QuestionTest(Integer id, Integer idQuestion, Integer idTest) {
        setId(id);
        setIdQuestion(idQuestion);
        setIdTest(idTest);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTest() {
        return idTest;
    }

    public void setIdTest(Integer idTest) {
        this.idTest = idTest;
    }

    public Integer getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }
}
