package com.testcreator.model;

public class TestCourse extends BaseModel {

    public final static String IDTEST = "idtest";
    public final static String IDCOURSE = "idcourse";

    private Integer idTest;
    private Integer idCourse;

    public TestCourse() {}

    public TestCourse(Integer id, Integer idTest, Integer idCourse) {
        setId(id);
        setIdTest(idTest);
        setIdCourse(idCourse);
    }

    public TestCourse(Integer idTest, Integer idCourse) {
        setIdTest(idTest);
        setIdCourse(idCourse);
    }

    public Integer getIdTest() {
        return idTest;
    }

    public void setIdTest(Integer idTest) {
        this.idTest = idTest;
    }

    public Integer getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Integer idCourse) {
        this.idCourse = idCourse;
    }
}
