package com.testcreator.db;

import com.testcreator.model.*;

import java.util.List;

public class Utils {

    private static final int COLSIZE = 40;

    private static String formatInfo(String info) {

        String formatted = "| ";

        String[] strArr = info.split("\\|");
        for (String item : strArr) {
            formatted += item;
            int size = item.length();
            for (int i = 0; i < (COLSIZE - size); i++) {
                formatted += " ";
            }
            formatted += "| ";
        }

        return formatted;
    }

    public static void printOut(List<?> list) {
        if (list != null && list.size() > 0) {
            String info = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) instanceof Student) {
                    Student student = (Student) list.get(i);
                    info = "" +
                            "ID: " + student.getId() + "|" +
                            "NAME: " + student.getName() + "|" +
                            "LASTNAME: " + student.getLastname();
                } else if (list.get(i) instanceof Instructor) {
                    Instructor instructor = (Instructor) list.get(i);
                    info = "" +
                            "ID: " + instructor.getId() + "|" +
                            "NAME: " + instructor.getName() + "|" +
                            "LASTNAME: " + instructor.getLastname();
                } else if (list.get(i) instanceof Question) {
                    Question question = (Question) list.get(i);
                    info = "" +
                            "ID: " + question.getId() + "|" +
                            "QUESTION: " + question.getQuestion() + "|" +
                            "ANSWER: " + question.isAnswer();
                } else if (list.get(i) instanceof Test) {
                    Test test = (Test) list.get(i);
                    info = "" +
                            "ID: " + test.getId() + "|" +
                            "NAME: " + test.getName() + "|" +
                            "IDINSTRUCTOR: " + test.getIdInstructor();
                } else if (list.get(i) instanceof StudentTest) {
                    StudentTest st = (StudentTest) list.get(i);
                    info = "" +
                            "ID: " + st.getId() + "|" +
                            "IDSTUDENT: " + st.getIdStudent() + "|" +
                            "IDTEST: " + st.getIdTest() + "|" +
                            "RESULT: " + st.getResult();
                } else if (list.get(i) instanceof Course) {
                    Course course = (Course) list.get(i);
                    info = "" +
                            "ID: " + course.getId() + "|" +
                            "NAME: " + course.getName() + "|" +
                            "IDINSTRUCTOR: " + course.getIdInstructor();
                }

                System.out.println(formatInfo(info));
            }
        }
    }

    public static void printOut(String message, Object object) {
        String info = "";
        if (object instanceof Student) {
            Student student = (Student) object;
            info = "" +
                    "ID: " + student.getId() + "|" +
                    "NAME: " + student.getName() + "|" +
                    "LASTNAME: " + student.getLastname();
        } else if (object instanceof Instructor) {
            Instructor instructor = (Instructor) object;
            info = "" +
                    "ID: " + instructor.getId() + "|" +
                    "NAME: " + instructor.getName() + "|" +
                    "LASTNAME: " + instructor.getLastname();
        } else if (object instanceof Question) {
            Question question = (Question) object;
            info = "" +
                    "ID: " + question.getId() + "|" +
                    "QUESTION: " + question.getQuestion() + "|" +
                    "ANSWER: " + question.isAnswer();
        } else if (object instanceof Test) {
            Test test = (Test) object;
            info = "" +
                    "ID: " + test.getId() + "|" +
                    "NAME: " + test.getName() + "|" +
                    "IDINSTRUCTOR: " + test.getIdInstructor();
        } else if (object instanceof Course) {
            Course course = (Course) object;
            info = "" +
                    "ID: " + course.getId() + "|" +
                    "NAME: " + course.getName() + "|" +
                    "IDINSTRUCTOR: " + course.getIdInstructor();
        } else if (object instanceof  StudentCourse) {
            StudentCourse sc = (StudentCourse) object;
            info = "" +
                    "ID: " + sc.getId() + "|" +
                    "IDSTUDENT: " + sc.getIdStudent() + "|" +
                    "IDCOURSE: " + sc.getIdCourse();
        } else if (object instanceof  QuestionTest) {
            QuestionTest qt = (QuestionTest) object;
            info = "" +
                    "ID: " + qt.getId() + "|" +
                    "IDTEST: " + qt.getIdTest() + "|" +
                    "IDQUESTION: " + qt.getIdQuestion();
        } else if (object instanceof  TestCourse) {
            TestCourse tc = (TestCourse) object;
            info = "" +
                    "ID: " + tc.getId() + "|" +
                    "IDTEST: " + tc.getIdTest() + "|" +
                    "IDCOURSE: " + tc.getIdCourse();
        }

        System.out.println(formatInfo(info));
    }
}
