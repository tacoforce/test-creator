package com.testcreator.db;

import java.sql.*;

public class Connector {

    private final static String CLASS_NAME_NEW = "com.mysql.jdbc.Driver";
    private final static String URL = "jdbc:mysql://localhost:3306/testcreator?useSSL=false";
    private final static String USR = "root";
    private final static String PWD = "";

    public Connection getConnection() {

        Connection conn = null;

        try {
            Class.forName(CLASS_NAME_NEW);
            conn = DriverManager.getConnection(URL, USR, PWD);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

}
