package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.QuestionTest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class QuestionTestRepository {

    private static final String NEW_QUESTION_TEST = "INSERT INTO question_test (idtest, idquestion) VALUES (?, ?)";

    private static final String ALL_QUESTIONS_FROM_TEST = "SELECT * FROM question_test WHERE idtest = ?";

    public QuestionTest insert(QuestionTest qt) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = null;
            ps = connection.prepareStatement(NEW_QUESTION_TEST, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, qt.getIdTest());
            ps.setInt(2, qt.getIdQuestion());

            return retrieveRecords(ps, qt);
        }

        return null;
    }

    public List<QuestionTest> selectAllQuestionsFromTest(Integer idTest) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = null;

            ps = connection.prepareStatement(ALL_QUESTIONS_FROM_TEST);
            ps.setInt(1, idTest);

            ResultSet rs = ps.executeQuery();

            List<QuestionTest> questionList = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    QuestionTest qt = new QuestionTest();
                    qt.setId(rs.getInt(QuestionTest.ID));
                    qt.setId(rs.getInt(QuestionTest.IDQUESTION));
                    qt.setId(rs.getInt(QuestionTest.IDTEST));

                    questionList.add(qt);
                }

                return questionList;
            }
        }
        return null;
    }

    private QuestionTest retrieveRecords(PreparedStatement ps, QuestionTest question) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                QuestionTest newQT = question;
                while (rs.next()) {
                    newQT.setId(rs.getInt(1));
                }

                printOut("QUESTION_TEST -> ", newQT);
                return newQT;
            }
        }
        return null;
    }
}
