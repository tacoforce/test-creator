package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.Instructor;
import com.testcreator.model.Student;
import com.testcreator.model.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class TestRepository {

    private static final String NEW_TEST = "INSERT INTO test (name, idinstructor) VALUES (?, ?)";
    private static final String ONE_COURSE = "SELECT * FROM course WHERE id = ?";

    private static final String ALL_TESTS_BY_INSTRUCTOR = "SELECT * FROM test WHERE idinstructor = ?";
    private static final String ALL_TESTS_BY_STUDENT = "" +
            "SELECT t.* FROM test t JOIN test_course tc ON t.id = tc.idtest WHERE tc.idcourse IN ( " +
            "SELECT c.id FROM course c JOIN student_course sc ON c.id = sc.idcourse AND sc.idstudent = ?) ";

    private static final String SINGLE_TEST_BY_STUDENT = "" +
            "SELECT t.* FROM test t JOIN test_course tc ON t.id = tc.idtest WHERE tc.idcourse IN ( " +
            "SELECT c.id FROM course c JOIN student_course sc ON c.id = sc.idcourse AND sc.idstudent = ?) " +
            "AND t.id = ?";

    public Test insert(Test test) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps;
            ps = connection.prepareStatement(NEW_TEST, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, test.getName());
            ps.setInt(2, test.getIdInstructor());

            return retrieveRecords(ps, test);
        }

        return null;
    }

    public Test getSingleTestByStudent(Integer idTest, Integer idStudent) throws SQLException {

        Connection connection = new Connector().getConnection();
        if (connection != null) {

            PreparedStatement ps = null;

            ps = connection.prepareStatement(SINGLE_TEST_BY_STUDENT);
            ps.setInt(1, idStudent);
            ps.setInt(2, idTest);

            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                Test test = null;

                while (rs.next()) {
                    test = new Test();
                    test.setId(rs.getInt(Test.ID));
                    test.setName(rs.getString(Test.NAME));
                    test.setIdInstructor(rs.getInt(Test.IDINSTRUCTOR));
                }

                printOut("TEST -> ", test);
                return test;
            }
        }

        return null;
    }

    public List<Test> getAllTestsBy(Integer id, Class entity) throws SQLException {

        List<Test> testList = new ArrayList<>();

        Connection connection = new Connector().getConnection();
        if (connection != null) {

            PreparedStatement ps = null;

            if (entity == Instructor.class) {
                ps = connection.prepareStatement(ALL_TESTS_BY_INSTRUCTOR);
                ps.setInt(1, id);
            } else if (entity == Student.class) {
                ps = connection.prepareStatement(ALL_TESTS_BY_STUDENT);
                ps.setInt(1, id);
            }

            ResultSet rs = ps.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    Test test = new Test();
                    test.setId(rs.getInt(Test.ID));
                    test.setName(rs.getString(Test.NAME));
                    test.setIdInstructor(rs.getInt(Test.IDINSTRUCTOR));
                    testList.add(test);
                }
            }
        }

        printOut(testList);
        return testList;
    }

    private Test retrieveRecords(PreparedStatement ps, Test test) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                Test newTest = test;
                while (rs.next()) {
                    newTest.setId(rs.getInt(1));
                }

                printOut("TEST -> ", newTest);
                return newTest;
            }
        }
        return null;
    }
}
