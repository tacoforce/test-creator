package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.Course;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class CourseRepository {

    private static final String NEW_COURSE = "INSERT INTO course (name, idinstructor) VALUES (?, ?)";
    private static final String ONE_COURSE = "SELECT * FROM course WHERE id = ?";
    private static final String ALL_COURSES_BY_INSTRUCTOR = "SELECT * FROM course WHERE idinstructor = ?";

    public Course insert(Course course) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps;
            ps = connection.prepareStatement(NEW_COURSE, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, course.getName());
            ps.setInt(2, course.getIdInstructor());

            return retrieveRecords(ps, course);
        }

        return null;
    }

    public Course selectOne(Integer id) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ONE_COURSE);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                Course course = null;

                while (rs.next()) {
                    course = new Course();
                    course.setId(rs.getInt(Course.ID));
                    course.setName(rs.getString(Course.NAME));
                    course.setIdInstructor(rs.getInt(Course.IDINSTRUCTOR));
                }

                printOut("COURSE -> ", course);
                return course;
            }
        }
        return null;
    }

    public List<Course> selectAllByInstructor(Integer idInstructor) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ALL_COURSES_BY_INSTRUCTOR);
            ps.setInt(1, idInstructor);

            ResultSet rs = ps.executeQuery();

            List<Course> courses = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Course course = new Course();
                    course.setId(rs.getInt(Course.ID));
                    course.setName(rs.getString(Course.NAME));
                    course.setIdInstructor(rs.getInt(Course.IDINSTRUCTOR));

                    courses.add(course);
                }

                printOut(courses);
                return courses;
            }
        }
        return null;
    }

    private Course retrieveRecords(PreparedStatement ps, Course course) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                Course newCourse = course;
                while (rs.next()) {
                    newCourse.setId(rs.getInt(1));
                }

                printOut("COURSE -> ", newCourse);
                return newCourse;
            }
        }
        return null;
    }
}
