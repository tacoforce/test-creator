package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class StudentRepository {

    private static final String NEW_STUDENT = "INSERT INTO student (name, lastname) VALUES (?, ?)";
    private static final String EDIT_STUDENT = "UPDATE student SET name = ?, lastname = ? WHERE id = ?";
    private static final String ALL_STUDENTS = "SELECT * FROM student";
    private static final String ONE_STUDENT = "SELECT * FROM student WHERE id = ?";

    public Student insert(Student student) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = connection.prepareStatement(NEW_STUDENT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, student.getName());
            ps.setString(2, student.getLastname());

            return retrieveRecords(ps, student);
        }

        return null;
    }

    public Student update(Integer id, Student student) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = connection.prepareStatement(EDIT_STUDENT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, student.getName());
            ps.setString(2, student.getLastname());
            ps.setInt(3, id);

            return retrieveRecords(ps, student);
        }
        return null;
    }

    public Student selectOne(Integer id) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ONE_STUDENT);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                Student student = null;

                while (rs.next()) {
                    student = new Student();
                    student.setId(rs.getInt(Student.ID));
                    student.setName(rs.getString(Student.NAME));
                    student.setLastname(rs.getString(Student.LASTNAME));
                }

                printOut("STUDENT -> ", student);
                return student;
            }
        }
        return null;
    }

    public List<Student> selectAll() throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ALL_STUDENTS);
            ResultSet rs = ps.executeQuery();

            List<Student> studentList = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Student student = new Student();
                    student.setId(rs.getInt(Student.ID));
                    student.setName(rs.getString(Student.NAME));
                    student.setLastname(rs.getString(Student.LASTNAME));

                    studentList.add(student);
                }

                printOut(studentList);
                return studentList;
            }
        }
        return null;
    }

    private Student retrieveRecords(PreparedStatement ps, Student student) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                Student newStudent = student;
                while (rs.next()) {
                    newStudent.setId(rs.getInt(1));
                }

                printOut("STUDENT -> ", newStudent);
                return newStudent;
            }
        }
        return null;
    }
}
