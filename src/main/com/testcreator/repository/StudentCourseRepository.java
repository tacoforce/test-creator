package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.StudentCourse;

import java.sql.*;

import static com.testcreator.db.Utils.printOut;

public class StudentCourseRepository {

    private static final String NEW_STUDENTCOURSE = "INSERT INTO student_course (idstudent, idcourse) VALUES (?, ?)";

    public StudentCourse insert(StudentCourse sc) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = null;
            ps = connection.prepareStatement(NEW_STUDENTCOURSE, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, sc.getIdStudent());
            ps.setInt(2, sc.getIdCourse());

            return retrieveRecords(ps, sc);
        }

        return null;
    }

    private StudentCourse retrieveRecords(PreparedStatement ps, StudentCourse sc) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                StudentCourse newSC = sc;
                while (rs.next()) {
                    newSC.setId(rs.getInt(1));
                }

                printOut("REGISTRATION -> ", newSC);
                return newSC;
            }
        }
        return null;
    }
}
