package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.TestCourse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class TestCourseRepository {

    private static final String NEW_TEST_COURSE = "INSERT INTO test_course (idtest, idcourse) VALUES (?, ?)";

    private static final String ALL_TESTS_BY_COURSE = "SELECT * FROM test_course WHERE idcourse = ?";

    public TestCourse insert(TestCourse tc) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps;
            ps = connection.prepareStatement(NEW_TEST_COURSE, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, tc.getIdTest());
            ps.setInt(2, tc.getIdCourse());

            return retrieveRecords(ps, tc);
        }

        return null;
    }

    public List<TestCourse> getAllTestsByCourse(Integer idCourse) throws SQLException {

        List<TestCourse> testList = new ArrayList<>();

        Connection connection = new Connector().getConnection();
        if (connection != null) {

            PreparedStatement ps = null;

            ps = connection.prepareStatement(ALL_TESTS_BY_COURSE);
            ps.setInt(1, idCourse);

            ResultSet rs = ps.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    TestCourse test = new TestCourse();
                    test.setId(rs.getInt(TestCourse.ID));
                    test.setIdTest(rs.getInt(TestCourse.IDTEST));
                    test.setIdCourse(rs.getInt(TestCourse.IDCOURSE));

                    testList.add(test);
                }
            }
        }

        printOut(testList);
        return testList;
    }

    private TestCourse retrieveRecords(PreparedStatement ps, TestCourse test) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                TestCourse newTest = test;
                while (rs.next()) {
                    newTest.setId(rs.getInt(1));
                }

                printOut("TEST_COURSE -> ", newTest);
                return newTest;
            }
        }
        return null;
    }
}
