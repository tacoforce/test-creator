package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.Instructor;
import com.testcreator.model.Question;
import com.testcreator.model.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class QuestionRepository {

    private static final String NEW_QUESTION = "INSERT INTO question (question, answer, idinstructor) VALUES (?, ?, ?)";
    private static final String ALL_QUESTIONS_FROM_INSTRUCTOR = "SELECT * FROM question WHERE idinstructor = ?";
    private static final String ALL_QUESTIONS_FROM_TEST = "" +
            "SELECT * FROM question WHERE id IN ( " +
            "SELECT idquestion FROM question_test WHERE idtest = ?)";

    public Question insert(Question question) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = null;
            ps = connection.prepareStatement(NEW_QUESTION, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, question.getQuestion());
            ps.setBoolean(2, question.isAnswer());
            ps.setInt(3, question.getIdInstructor());

            return retrieveRecords(ps, question);
        }

        return null;
    }

    public List<Question> selectAllFrom(Integer id, Class entity) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = null;

            if (entity == Instructor.class) {
                ps = connection.prepareStatement(ALL_QUESTIONS_FROM_INSTRUCTOR);
                ps.setInt(1, id);
            } else if (entity == Test.class) {
                ps = connection.prepareStatement(ALL_QUESTIONS_FROM_TEST);
                ps.setInt(1, id);
            }

            ResultSet rs = ps.executeQuery();

            List<Question> questionList = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Question question = new Question();
                    question.setId(rs.getInt(Question.ID));
                    question.setQuestion(rs.getString(Question.QUESTION));
                    question.setIdInstructor(rs.getInt(Question.IDINSTRUCTOR));
                    question.setAnswer(rs.getBoolean(Question.ANSWER));

                    questionList.add(question);
                }

                if (entity != Test.class) {
                    printOut(questionList);
                }
                return questionList;
            }
        }
        return null;
    }

    private Question retrieveRecords(PreparedStatement ps, Question question) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                Question newQuestion = question;
                while (rs.next()) {
                    newQuestion.setId(rs.getInt(1));
                }

                printOut("QUESTION -> ", newQuestion);
                return newQuestion;
            }
        }
        return null;
    }
}
