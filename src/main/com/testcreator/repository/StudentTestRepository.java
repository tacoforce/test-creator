package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.StudentTest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class StudentTestRepository {

    private static final String NEW_STUDENTTEST = "INSERT INTO student_test (idstudent, idtest, result) VALUES (?, ?, ?)";
    private static final String ALL_STUDENT_RESULTS = "SELECT * FROM student_test WHERE idstudent = ?";

    public StudentTest insert(StudentTest st) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = null;
            ps = connection.prepareStatement(NEW_STUDENTTEST, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, st.getIdStudent());
            ps.setInt(2, st.getIdTest());
            ps.setInt(3, st.getResult());

            return retrieveRecords(ps, st);
        }

        return null;
    }

    public List<StudentTest> selectAll(Integer idStudent) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ALL_STUDENT_RESULTS);
            ps.setInt(1, idStudent);

            ResultSet rs = ps.executeQuery();

            List<StudentTest> studentTests = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    StudentTest studentTest = new StudentTest();
                    studentTest.setId(rs.getInt(StudentTest.ID));
                    studentTest.setIdStudent(rs.getInt(StudentTest.IDSTUDENT));
                    studentTest.setIdTest(rs.getInt(StudentTest.IDTEST));
                    studentTest.setResult(rs.getInt(StudentTest.RESULT));

                    studentTests.add(studentTest);
                }

                printOut(studentTests);
                return studentTests;
            }
        }
        return null;
    }

    private StudentTest retrieveRecords(PreparedStatement ps, StudentTest st) throws SQLException {

        if (ps.executeUpdate() > 0) {
            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                StudentTest newSC = st;
                while (rs.next()) {
                    newSC.setId(rs.getInt(1));
                }

                printOut("RESULT -> ", newSC);
                return newSC;
            }
        }
        return null;
    }
}
