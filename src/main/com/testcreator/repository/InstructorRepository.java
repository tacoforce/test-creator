package com.testcreator.repository;

import com.testcreator.db.Connector;
import com.testcreator.model.Instructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.testcreator.db.Utils.printOut;

public class InstructorRepository {

    private static final String NEW_INSTRUCTOR = "INSERT INTO instructor (name, lastname) VALUES (?, ?)";
    private static final String EDIT_INSTRUCTOR = "UPDATE instructor SET name = ?, lastname = ? WHERE id = ?";
    private static final String ALL_INSTRUCTORS = "SELECT * FROM instructor";
    private static final String ONE_INSTRUCTOR = "SELECT * FROM instructor WHERE id = ?";

    public Instructor insert(Instructor instructor) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = connection.prepareStatement(NEW_INSTRUCTOR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, instructor.getName());
            ps.setString(2, instructor.getLastname());

            return retrieveRecords(ps, instructor);
        }

        return null;
    }

    public Instructor update(Integer id, Instructor instructor) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {
            PreparedStatement ps = connection.prepareStatement(EDIT_INSTRUCTOR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, instructor.getName());
            ps.setString(2, instructor.getLastname());
            ps.setInt(3, id);

            return retrieveRecords(ps, instructor);
        }
        return null;
    }

    public Instructor selectOne(Integer id) throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ONE_INSTRUCTOR);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                Instructor instructor = null;

                while (rs.next()) {
                    instructor = new Instructor();
                    instructor.setId(rs.getInt(Instructor.ID));
                    instructor.setName(rs.getString(Instructor.NAME));
                    instructor.setLastname(rs.getString(Instructor.LASTNAME));
                }

                printOut("INSTRUCTOR -> ", instructor);
                return instructor;
            }
        }
        return null;
    }

    public List<Instructor> selectAll() throws SQLException {

        Connection connection = new Connector().getConnection();

        if (connection != null) {

            PreparedStatement ps = connection.prepareStatement(ALL_INSTRUCTORS);
            ResultSet rs = ps.executeQuery();

            List<Instructor> instructorList = new ArrayList<>();

            if (rs != null) {
                while (rs.next()) {
                    Instructor instructor = new Instructor();
                    instructor.setId(rs.getInt(Instructor.ID));
                    instructor.setName(rs.getString(Instructor.NAME));
                    instructor.setLastname(rs.getString(Instructor.LASTNAME));

                    instructorList.add(instructor);
                }

                printOut(instructorList);
                return instructorList;
            }
        }
        return null;
    }

    private Instructor retrieveRecords(PreparedStatement ps, Instructor instructor) throws SQLException {

        if (ps.executeUpdate() > 0) {

            ResultSet rs = ps.getGeneratedKeys();

            if (rs != null) {

                Instructor newInstructor = instructor;
                while (rs.next()) {
                    newInstructor.setId(rs.getInt(1));
                }

                printOut("INSTRUCTOR -> ", newInstructor);
                return newInstructor;
            }
        }
        return null;
    }
}
