package com.testcreator.user;

import com.testcreator.model.Instructor;
import com.testcreator.model.Student;
import com.testcreator.service.InstructorService;
import com.testcreator.service.StudentService;

import java.sql.SQLException;
import java.util.List;

public class AdminUser {

    private InstructorService instructorService = new InstructorService();
    private StudentService studentService = new StudentService();

    public Instructor addInstructor(String name, String lastname) {
        try {
            return instructorService.insertInstructor(name, lastname);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Instructor: " + e.getMessage());
        }
        return null;
    }

    public Student addStudent(String name, String lastname) {
        try {
            return studentService.insertStudent(name, lastname);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Student: " + e.getMessage());
        }
        return null;
    }

    public List<Instructor> getAllInstructors() {
        try {
            return instructorService.getInstructors();
        } catch (SQLException e) {
            System.out.print("An error occurred while retrieving Instructors: " + e.getMessage());
        }
        return null;
    }

    public List<Student> getAllStudents() {
        try {
            return studentService.getStudents();
        } catch (SQLException e) {
            System.out.print("An error occurred while retrieving Students: " + e.getMessage());
        }
        return null;
    }
}
