package com.testcreator.user;

import com.testcreator.model.*;
import com.testcreator.service.*;

import java.sql.SQLException;
import java.util.List;

public class InstructorUser {

    private InstructorService instructorService = new InstructorService();
    private StudentService studentService = new StudentService();
    private QuestionService questionService = new QuestionService();
    private TestService testService = new TestService();
    private CourseService courseService = new CourseService();

    public Instructor getInstructor(Integer id) {
        try {
            return instructorService.getInstructor(id);
        } catch (SQLException e) {
            System.out.print("An error occurred while retrieving the Instructor (" + id + "): " + e.getMessage());
        }
        return null;
    }

    public Instructor updateInstructor(Integer id, String name, String lastname) {
        try {
            return instructorService.updateInstructor(id, new Instructor(id, name, lastname));
        } catch (SQLException e) {
            System.out.print("An error occurred while editing Instructor (" + id + "): " + e.getMessage());
        }
        return null;
    }

    public Question createQuestion(String question, boolean answer, Integer idInstructor) {
        try {
            return questionService.addQuestion(question, answer, idInstructor);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Question: " + e.getMessage());
        }
        return null;
    }

    public Test createTest(String name, Integer idInstructor) {
        try {
            return testService.addTest(name, idInstructor);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Test: " + e.getMessage());
        }
        return null;
    }

    public void linkQuestionToTest(Integer idQuestion, Integer idTest) {
        try {
            questionService.linkQuestionToTest(idQuestion, idTest);
        } catch (SQLException e) {
            System.out.print("An error occurred while linking the Question to the Test: " + e.getMessage());
        }
    }

    public void linkTestToCourse(Integer idTest, Integer idCourse) {
        try {
            testService.linkTestToCourse(idTest, idCourse);
        } catch (SQLException e) {
            System.out.print("An error occurred while linking the Test to the Course: " + e.getMessage());
        }
    }

    public Course createCourse(String name, Integer idInstructor) {
        try {
            return courseService.addCourse(name, idInstructor);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Course: " + e.getMessage());
        }
        return null;
    }

    public void registerStudent(Integer idStudent, Integer idCourse) {
        try {
            if (studentService.getStudent(idStudent) == null) {
                System.out.print("Student record doesn't exist with ID: {" + idStudent + "}\n");
                return;
            }
            if (courseService.getCourse(idCourse) == null) {
                System.out.print("Course record doesn't exist with ID: {" + idCourse + "}\n");
                return;
            }
            instructorService.registerStudentToCourse(idStudent, idCourse);
        } catch (SQLException e) {
            System.out.print("An error occurred while creating Registering the Student into the Course: " + e.getMessage());
        }
    }

    public List<Question> getAllQuestions(Integer idInstructor) {
        try {
            return questionService.getQuestionsFrom(idInstructor, Instructor.class);
        } catch (SQLException e) {
            System.out.print("There's not Questions created yet: " + e.getMessage());
        }
        return null;
    }

    public List<Test> getAllTests(Integer idInstructor) {
        try {
            return testService.getTestsByInstructor(idInstructor);
        } catch (SQLException e) {
            System.out.print("There's not Questions created yet: " + e.getMessage());
        }
        return null;
    }

    public List<Course> getAllCourses(Integer idInstructor) {
        try {
            return courseService.getAllCoursesByInstructor(idInstructor);
        } catch (SQLException e) {
            System.out.print("There's not Questions created yet: " + e.getMessage());
        }
        return null;
    }

    public List<Student> getAllStudents() {
        try {
            return studentService.getStudents();
        } catch (SQLException e) {
            System.out.print("An error occurred while retrieving Students: " + e.getMessage());
        }
        return null;
    }
}
