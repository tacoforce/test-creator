package com.testcreator.user;

import com.testcreator.model.Question;
import com.testcreator.model.Student;
import com.testcreator.model.StudentTest;
import com.testcreator.model.Test;
import com.testcreator.service.QuestionService;
import com.testcreator.service.StudentService;
import com.testcreator.service.TestService;

import java.sql.SQLException;
import java.util.List;

public class StudentUser {

    private StudentService studentService = new StudentService();
    private TestService testService = new TestService();
    private QuestionService questionService = new QuestionService();

    public Student getStudent(Integer id) {
        try {
            return studentService.getStudent(id);
        } catch (SQLException e) {
            System.out.print("An error occurred while retrieving the Student (" + id + "): " + e.getMessage());
        }
        return null;
    }

    public Student editStudent(Integer id, String name, String lastname) {
        try {
            return studentService.updatetStudent(id, new Student(id, name, lastname));
        } catch (SQLException e) {
            System.out.print("An error occurred while editing Student (" + id + "): " + e.getMessage());
        }
        return null;
    }

    public Test getTest(Integer idStudent, Integer idTest) {
        try {
            return testService.getSingleTestByStudent(idStudent, idTest);
        } catch (SQLException e) {
            System.out.print("Student is not allowed to answer Test ID {" + idTest + "}: " + e.getMessage());
        }
        return null;
    }

    public List<Test> getTests(Integer idStudent) {
        try {
            return testService.getAllTestsByStudent(idStudent);
        } catch (SQLException e) {
            System.out.print("Student is not enrolled in any course: " + e.getMessage());
        }
        return null;
    }

    public List<Question> getQuestionsFromTest(Integer idTest) {
        try {
            return questionService.getQuestionsFrom(idTest, Test.class);
        } catch (SQLException e) {
            System.out.println("Test {" + idTest + "} doesn't have any Questions yet " + e.getMessage());
        }
        return null;
    }

    public void storeResult(Integer idStudent, Integer idTest, Integer result) {
        try {
            studentService.storeResult(idStudent, idTest, result);
        } catch (SQLException e) {
            System.out.println("An error occurred while storing Student's result: " + e.getMessage());
        }
    }

    public List<StudentTest> getAllResults(Integer idStudent) {
        try {
            return studentService.getResults(idStudent);
        } catch (SQLException e) {
            System.out.println("An error occurred while retrieving Student results: " + e.getMessage());
        }
        return null;
    }
}
