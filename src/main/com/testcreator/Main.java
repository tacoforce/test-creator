package com.testcreator;

import com.testcreator.model.Instructor;
import com.testcreator.model.Question;
import com.testcreator.model.Student;
import com.testcreator.model.Test;
import com.testcreator.user.AdminUser;
import com.testcreator.user.InstructorUser;
import com.testcreator.user.StudentUser;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

    private void adminServices() {

        AdminUser adminUser = new AdminUser();
        boolean isRunning = true;
        Scanner scanner = new Scanner(System.in);

        while (isRunning) {

            System.out.println("\n" +
                    "0. Exit Admin Services \n" +
                    "1. Add an Instructor \n" +
                    "2. Add an Student \n" +
                    "3. List all Students \n" +
                    "4. List all Instructors \n" +
                    "\nSelect a Service: \n");

            try {
                int input = scanner.nextInt();

                if (input >= 0 && input <= 4) {
                    switch (input) {
                        case 0: {
                            isRunning = false;
                            break;
                        }
                        case 1: {
                            System.out.println("--- Create a new Instructor record ---");

                            System.out.println("Type Instructor's Name: ");
                            String name = scanner.next();
                            System.out.println("Type Last Name: ");
                            String lastName = scanner.next();
                            adminUser.addInstructor(name, lastName);
                            break;
                        }
                        case 2: {
                            System.out.println("--- Create a new Student record ---");

                            System.out.println("Type Student's Name: ");
                            String name = scanner.next();
                            System.out.println("Type Last Name: ");
                            String lastName = scanner.next();
                            adminUser.addStudent(name, lastName);
                            break;
                        }
                        case 3: {
                            System.out.println("--- List of all Student ---");
                            adminUser.getAllStudents();
                            System.out.println("--- End of List ---");
                            break;
                        }
                        case 4: {
                            System.out.println("--- List of all Instructors ---");
                            adminUser.getAllInstructors();
                            System.out.println("--- End of List ---");
                            break;
                        }
                    }
                }
            } catch(InputMismatchException e) {
                scanner.nextLine();
            }
        }
    }

    private void instructorServices() {

        InstructorUser instructorUser = new InstructorUser();
        boolean isRunning = true;
        Scanner scanner = new Scanner(System.in);

        Instructor instructor = isInstructor(instructorUser);

        if (instructor != null) {
            while (isRunning) {

                System.out.println("\n" +
                        "0. Exit Instructor Services \n" +
                        "1. Edit Instructor's Information \n" +
                        "2. Create a Question \n" +
                        "3. Create a Test \n" +
                        "4. Create a Course \n" +
                        "5. Link a Question to a Test \n" +
                        "6. Link a Test to a Course \n" +
                        "7. Register a Student to a Course \n" +
                        "\nSelect a Service: \n");

                try {
                    int input = scanner.nextInt();

                    if (input >= 0 && input <= 7) {
                        switch (input) {
                            case 0: {
                                isRunning = false;
                                break;
                            }
                            case 1: {
                                System.out.println("--- Edit Instructor's Information ---");

                                System.out.println("Type Instructor's new Name: ");
                                String name = scanner.next();
                                System.out.println("Type Last Name: ");
                                String lastName = scanner.next();
                                instructorUser.updateInstructor(instructor.getId(), name, lastName);
                                break;
                            }
                            case 2: {
                                System.out.println("--- Create a new Question ---");

                                System.out.println("Type in the Question (Must be of type True or False): ");
                                String question = scanner.next();
                                System.out.println("Type the Answer {T} for True, {F} for False: ");
                                String strAnswer = scanner.next();

                                if (strAnswer.equalsIgnoreCase("T") || strAnswer.equalsIgnoreCase("F")) {
                                    boolean answer = strAnswer.equalsIgnoreCase("T") ? true : false;
                                    instructorUser.createQuestion(question, answer, instructor.getId());
                                } else {
                                    System.out.println("Incorrect answer. Try to create a new Question.");
                                }

                                break;
                            }
                            case 3: {
                                System.out.println("--- Create a new Test ---");

                                System.out.println("Type a Test title: ");
                                String name = scanner.nextLine();
                                instructorUser.createTest(name, instructor.getId());
                                break;
                            }
                            case 4: {
                                System.out.println("--- Create a new Course ---");

                                System.out.println("Type the Course name: ");
                                String name = scanner.next();

                                instructorUser.createCourse(name, instructor.getId());
                                break;
                            }
                            case 5: {
                                System.out.println("--- Link a Question to a Test ---");

                                System.out.println("--- List of all Instructor's Questions ---");
                                List questionsList = instructorUser.getAllQuestions(instructor.getId());
                                System.out.println("--- End of List ---");

                                if (questionsList != null && questionsList.size() > 0) {
                                    System.out.println("Type the Question's ID: ");
                                    Integer idQuestion = scanner.nextInt();

                                    System.out.println("--- List of all Instructor's Tests ---");
                                    List testsList = instructorUser.getAllTests(instructor.getId());
                                    System.out.println("--- End of List ---");

                                    if (testsList != null && testsList.size() > 0) {
                                        System.out.println("Type the Test's ID: ");
                                        Integer idTest = scanner.nextInt();

                                        instructorUser.linkQuestionToTest(idQuestion, idTest);
                                    } else {
                                        System.out.println("The Instructor hasn't created Tests yet.");
                                    }
                                } else {
                                    System.out.println("The Instructor hasn't created Questions yet.");
                                }

                                break;
                            }
                            case 6: {
                                System.out.println("--- Link a Test to a Course ---");

                                System.out.println("--- List of all Instructor's Tests ---");
                                List testsList = instructorUser.getAllTests(instructor.getId());
                                System.out.println("--- End of List ---\n");

                                if (testsList != null && testsList.size() > 0) {
                                    System.out.println("Type the Test's ID: ");
                                    Integer idTest = scanner.nextInt();

                                    System.out.println("--- List of all Instructor's Courses ---");
                                    List courseLists = instructorUser.getAllCourses(instructor.getId());
                                    System.out.println("--- End of List ---\n");

                                    if (courseLists != null && courseLists.size() > 0) {
                                        System.out.println("Type the Course's ID: ");
                                        Integer idCourse = scanner.nextInt();

                                        instructorUser.linkTestToCourse(idTest, idCourse);
                                    } else {
                                        System.out.println("The Instructor hasn't created Courses yet.");
                                    }
                                } else {
                                    System.out.println("The Instructor hasn't created Tests yet.");
                                }

                                break;
                            }
                            case 7: {
                                System.out.println("--- Register a Student to a Course ---");

                                System.out.println("\n--- List of all Student ---");
                                instructorUser.getAllStudents();
                                System.out.println("--- End of List ---\n");

                                System.out.println("Type the Student's ID: ");
                                Integer idStudent = scanner.nextInt();

                                System.out.println("\n--- List of all Instructor's Courses ---");
                                List courseLists = instructorUser.getAllCourses(instructor.getId());
                                System.out.println("--- End of List ---\n");

                                System.out.println("Type the Course's ID: ");
                                Integer idCourse = scanner.nextInt();

                                instructorUser.registerStudent(idStudent, idCourse);
                                break;
                            }
                        }
                    }
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                }
            }
        }
    }

    private void studentServices() {

        StudentUser studentUser = new StudentUser();
        boolean isRunning = true;
        Scanner scanner = new Scanner(System.in);

        Student student = isStudent(studentUser);
        Test test = null;

        if (student != null) {
            while (isRunning) {

                System.out.println("\n" +
                        "0. Exit Student's Services \n" +
                        "1. Edit Student's Information \n" +
                        "2. Select a Test \n" +
                        "3. Solve a Test \n" +
                        "4. View Results \n" +
                        "\nSelect a Service: \n");

                try {
                    int input = scanner.nextInt();

                    if (input >= 0 && input <= 5) {
                        switch (input) {
                            case 0: {
                                isRunning = false;
                                break;
                            }
                            case 1: {
                                System.out.println("--- Edit Student's Information ---");

                                System.out.println("Type Student's new Name: ");
                                String name = scanner.next();
                                System.out.println("Type Last Name: ");
                                String lastName = scanner.next();
                                studentUser.editStudent(student.getId(), name, lastName);
                                break;
                            }
                            case 2: {
                                System.out.println("--- Select a Test ---");

                                System.out.println("These are the Tests of the Courses you are enrolled to:");
                                studentUser.getTests(student.getId());

                                System.out.println("\nType in the Test ID you would like to solve:");
                                int idTest = scanner.nextInt();

                                test = studentUser.getTest(student.getId(), idTest);

                                break;
                            }
                            case 3: {
                                System.out.println("--- Solve a Test ---");

                                if (test == null) {
                                    System.out.println("You haven't selected a Test");
                                } else {
                                    System.out.println("You have selected Test ID {" + test.getId() + "}: " + test.getName());

                                    List<Question> questionList = studentUser.getQuestionsFromTest(test.getId());

                                    int correctAnswers = 0;
                                    int questionCounter = 1;

                                    for (Question question : questionList) {

                                        System.out.println("\n" + questionCounter + ". " + question.getQuestion());

                                        String strAnswer = "";

                                        while (!strAnswer.equalsIgnoreCase("T") && !strAnswer.equalsIgnoreCase("F")) {
                                            System.out.println("Type the Answer {T} for True, {F} for False: ");
                                            strAnswer = scanner.next();

                                            if (strAnswer.equalsIgnoreCase("T") || strAnswer.equalsIgnoreCase("F")) {
                                                boolean answer = strAnswer.equalsIgnoreCase("T") ? true : false;

                                                if (answer == question.isAnswer()) {
                                                    correctAnswers++;
                                                }
                                            }
                                        }
                                    }

                                    int score = (correctAnswers * 100) / questionList.size();
                                    System.out.println("\n--- Test Results ---");
                                    System.out.println("You have " + correctAnswers + " correct answers of " + questionList.size());
                                    System.out.println("Your score is: " + score + " of 100");

                                    studentUser.storeResult(student.getId(), test.getId(), score);
                                }
                                break;
                            }
                            case 4: {
                                System.out.println("--- Student's Test Results ---");
                                studentUser.getAllResults(student.getId());
                                System.out.println("--- End of List ---");
                                break;
                            }
                        }
                    }
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                }
            }
        }
    }

    private Instructor isInstructor(InstructorUser instructorUser) {

        System.out.println("Type your Instructor ID: ");

        Scanner scanner = new Scanner(System.in);
        int idInstructor = scanner.nextInt();

        Instructor instructor = instructorUser.getInstructor(idInstructor);

        if (instructor != null) {
            System.out.println("\nWelcome " + instructor.getName() + " " + instructor.getLastname());
            return instructor;
        } else {
            System.out.println("Instructor not found with id: {" + idInstructor + "}");
        }

        return null;
    }

    private Student isStudent(StudentUser studentUser) {

        System.out.println("Type your Student ID: ");

        Scanner scanner = new Scanner(System.in);
        int idStudent = scanner.nextInt();

        Student student = studentUser.getStudent(idStudent);

        if (student != null) {
            System.out.println("\nWelcome " + student.getName() + " " + student.getLastname());
            return student;
        } else {
            System.out.println("Instructor not found with id: {" + idStudent + "}");
        }

        return null;
    }

    public static void main(String[] args) {

        Main main = new Main();
        boolean isRunning = true;
        Scanner scanner = new Scanner(System.in);

        while (isRunning) {

            System.out.println("\n" +
                    "----- Test Creator Application -----\n" +
                    "0. Exit \n" +
                    "1. Admin \n" +
                    "2. Instructor \n" +
                    "3. Student \n" +
                    "\nSelect an option: \n");

            try {
                int input = scanner.nextInt();

                if (input >= 0 && input <= 3) {
                    switch (input) {
                        case 0: {
                            isRunning = false;
                            break;
                        }
                        case 1: {
                            System.out.println("----- Admin Services -----");
                            main.adminServices();
                            break;
                        }
                        case 2: {
                            System.out.println("----- Instructor Services -----");
                            main.instructorServices();
                            break;
                        }
                        case 3: {
                            System.out.println("----- Student Services -----");
                            main.studentServices();
                            break;
                        }
                    }
                }
            } catch(InputMismatchException e) {
                scanner.nextLine();
            }
        }
    }
}
