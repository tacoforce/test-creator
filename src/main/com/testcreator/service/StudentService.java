package com.testcreator.service;

import com.testcreator.model.Student;
import com.testcreator.model.StudentTest;
import com.testcreator.repository.StudentRepository;
import com.testcreator.repository.StudentTestRepository;

import java.sql.SQLException;
import java.util.List;

public class StudentService {

    private StudentRepository studentRepository = new StudentRepository();
    private StudentTestRepository studentTestRepository = new StudentTestRepository();

    public Student insertStudent(String name, String lastname) throws SQLException {
        return studentRepository.insert(new Student(name, lastname));
    }

    public Student updatetStudent(Integer id, Student student) throws SQLException {
        return studentRepository.update(id, student);
    }

    public Student getStudent(Integer id) throws SQLException {
        return studentRepository.selectOne(id);
    }

    public List<Student> getStudents() throws SQLException {
        return studentRepository.selectAll();
    }

    public void storeResult(Integer idStudent, Integer idTest, Integer result) throws SQLException {
        studentTestRepository.insert(new StudentTest(idStudent, idTest, result));
    }

    public List<StudentTest> getResults(Integer idStudent) throws SQLException {
        return studentTestRepository.selectAll(idStudent);
    }
}
