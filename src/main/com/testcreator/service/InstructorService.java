package com.testcreator.service;

import com.testcreator.model.Instructor;
import com.testcreator.model.StudentCourse;
import com.testcreator.repository.InstructorRepository;
import com.testcreator.repository.StudentCourseRepository;

import java.sql.SQLException;
import java.util.List;

public class InstructorService {

    private InstructorRepository instructorRepository = new InstructorRepository();
    private StudentCourseRepository studentCourseRepository = new StudentCourseRepository();

    public Instructor insertInstructor(String name, String lastname) throws SQLException {
        return instructorRepository.insert(new Instructor(name, lastname));
    }

    public Instructor updateInstructor(Integer id, Instructor instructor) throws SQLException {
        return instructorRepository.update(id, instructor);
    }

    public Instructor getInstructor(Integer id) throws SQLException {
        return instructorRepository.selectOne(id);
    }

    public List<Instructor> getInstructors() throws SQLException {
        return instructorRepository.selectAll();
    }

    public void registerStudentToCourse(Integer idStudent, Integer idCourse) throws SQLException {
        studentCourseRepository.insert(new StudentCourse(idStudent, idCourse));
    }
}
