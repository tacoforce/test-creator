package com.testcreator.service;

import com.testcreator.model.Course;
import com.testcreator.repository.CourseRepository;

import java.sql.SQLException;
import java.util.List;

public class CourseService {

    private CourseRepository courseRepository = new CourseRepository();

    public Course addCourse(String name, Integer idInstructor) throws SQLException {
        return courseRepository.insert(new Course(name, idInstructor));
    }

    public Course getCourse(Integer idCourse) throws SQLException {
        return courseRepository.selectOne(idCourse);
    }

    public List<Course> getAllCoursesByInstructor(Integer idInstructor) throws SQLException {
        return courseRepository.selectAllByInstructor(idInstructor);
    }
}
