package com.testcreator.service;

import com.testcreator.model.*;
import com.testcreator.repository.TestCourseRepository;
import com.testcreator.repository.TestRepository;

import java.sql.SQLException;
import java.util.List;

public class TestService {

    private TestRepository testRepository = new TestRepository();
    private TestCourseRepository testCourseRepository = new TestCourseRepository();

    public Test addTest(String name, Integer idInstructor) throws SQLException {
        return testRepository.insert(new Test(name, idInstructor));
    }

    public List<Test> getTestsByInstructor(Integer idInstructor) throws SQLException {
        return testRepository.getAllTestsBy(idInstructor, Instructor.class);
    }

    public Test getSingleTestByStudent(Integer idStudent, Integer idTest) throws SQLException {
        return testRepository.getSingleTestByStudent(idTest, idStudent);
    }

    public List<Test> getAllTestsByStudent(Integer idStudent) throws SQLException {
        return testRepository.getAllTestsBy(idStudent, Student.class);
    }

    public void linkTestToCourse(Integer idTest, Integer idCourse) throws SQLException {
        testCourseRepository.insert(new TestCourse(idTest, idCourse));
    }
}
