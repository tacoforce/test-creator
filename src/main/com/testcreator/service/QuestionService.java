package com.testcreator.service;

import com.testcreator.model.Question;
import com.testcreator.model.QuestionTest;
import com.testcreator.repository.QuestionRepository;
import com.testcreator.repository.QuestionTestRepository;

import java.sql.SQLException;
import java.util.List;

public class QuestionService {

    private QuestionRepository questionRepository = new QuestionRepository();
    private QuestionTestRepository questionTestRepository = new QuestionTestRepository();

    public Question addQuestion(String question, boolean answer, Integer idInstructor) throws SQLException {
        return questionRepository.insert(new Question(question, answer, idInstructor));
    }

    public List<Question> getQuestionsFrom(Integer id, Class entity) throws SQLException {
        return questionRepository.selectAllFrom(id, entity);
    }

    public void linkQuestionToTest(Integer idQuestion, Integer idTest) throws SQLException {
        questionTestRepository.insert(new QuestionTest(idQuestion, idTest));
    }

}
