package com.testcreator.service;

import com.testcreator.model.Course;
import com.testcreator.model.Instructor;
import com.testcreator.model.Question;
import com.testcreator.model.Student;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestServices {

    @Test
    public void testCourseService() {

        CourseService courseService = new CourseService();

        try {
            Course newCourse = courseService.addCourse("Test", 1);
            assertNotNull(newCourse);

            Course course = courseService.getCourse(1);
            assertNotNull(course);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testQuestionService() {

        QuestionService questionService = new QuestionService();

        try {
            Question question = questionService.addQuestion("Question", true, 1);
            assertNotNull(question);

            // Get Instructor Questions
            assertNotNull(questionService.getQuestionsFrom(1, Instructor.class));

            // Get Test Questions
            assertNotNull(questionService.getQuestionsFrom(1, com.testcreator.model.Test.class));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testTestService() {

        com.testcreator.service.TestService testService = new com.testcreator.service.TestService();

        try {
            com.testcreator.model.Test test = testService.addTest("Test", 1);
            assertNotNull(test);

            assertNotNull(testService.getTestsByInstructor(1));

            assertNotNull(testService.getAllTestsByStudent(1));

            assertNotNull(testService.getSingleTestByStudent(1, 1));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testStudentService() {

        StudentService studentService = new StudentService();

        try {
            Student student = studentService.insertStudent("Test", "Student");
            assertNotNull(student);

            assertNotNull(studentService.getStudent(student.getId()));

            student = studentService.updatetStudent(
                student.getId(),
                new Student("NewTest", "NewLastName")
            );

            assertNotNull(student);

            assertNotNull(studentService.getStudents());

            studentService.storeResult(1, 1, 100);

            assertNotNull(studentService.getResults(1));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInstructorService() {

        InstructorService instructorService = new InstructorService();

        try {
            Instructor newInstructor = instructorService.insertInstructor("Test", "Instructor");
            assertNotNull(newInstructor);

            newInstructor = instructorService.updateInstructor(
                    newInstructor.getId(),
                    new Instructor("NewName", "NewLastName"));
            assertNotNull(newInstructor);

            Instructor existing = instructorService.getInstructor(1);
            assertNotNull(existing);

            assertNotNull(instructorService.getInstructors());

            instructorService.registerStudentToCourse(1, 1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
