package com.testcreator.user;

import com.testcreator.model.Instructor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestUsers {

    @Test
    public void testAdminUser() {

        AdminUser user = new AdminUser();

        try {
            user.addInstructor("Test", "Instructor");
            user.addStudent("Tests", "Student");
            user.getAllInstructors();
            user.getAllStudents();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInstructorUser() {

        InstructorUser user = new InstructorUser();

        try {
            assertNotNull(user.createCourse("Course", 1));
            assertNotNull(user.createTest("Test", 1));
            assertNotNull(user.createQuestion("Question", true, 1));

            Instructor instructor = user.getInstructor(1);
            assertNotNull(instructor);

            instructor = user.updateInstructor(1, "NewName", "NewLastName");
            assertNotNull(instructor);

            user.linkQuestionToTest(1, 1 );

            user.registerStudent(1, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStudentUser() {

        StudentUser user = new StudentUser();

        try {
            assertNotNull(user.getTest(1, 1));
            assertNotNull(user.getTests(1));
            assertNotNull(user.getAllResults(1));
            assertNotNull(user.getQuestionsFromTest(1));
            assertNotNull(user.getStudent(1));
            assertNotNull(user.editStudent(1, "NewName", "NewLastName"));
            user.storeResult(1, 1, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
