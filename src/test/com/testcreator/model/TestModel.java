package com.testcreator.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestModel {

    @Test
    public void testCourse() {

        Course course = new Course(1, "TestModel", 2);

        Course newCourse = new Course();
        newCourse.setId(course.getId());
        newCourse.setName(course.getName());
        newCourse.setIdInstructor(course.getIdInstructor());

        Course anotherCourse = new Course(newCourse.getName(), newCourse.getIdInstructor());

        assertNotNull(course);
        assertNotNull(newCourse);
        assertNotNull(anotherCourse);
    }

    @Test
    public void testInstructor() {

        Instructor instructor = new Instructor(1, "Test", "Instructor");

        Instructor newInstructor = new Instructor();
        newInstructor.setId(instructor.getId());
        newInstructor.setName(instructor.getName());
        newInstructor.setLastname(instructor.getLastname());

        Instructor another = new Instructor(instructor.getName(), instructor.getLastname());

        assertNotNull(instructor);
        assertNotNull(newInstructor);
        assertNotNull(another);
    }

    @Test
    public void testQuestion() {

        Question question = new Question(1, "Question", true, 2);

        Question newQuestion = new Question();
        newQuestion.setId(question.getId());
        newQuestion.setQuestion(question.getQuestion());
        newQuestion.setAnswer(question.isAnswer());
        newQuestion.setIdInstructor(question.getIdInstructor());

        Question another = new Question(question.getQuestion(), question.isAnswer(), question.getIdInstructor());

        assertNotNull(question);
        assertNotNull(newQuestion);
        assertNotNull(another);
    }

    @Test
    public void testStudent() {

        Student student = new Student(1, "Test", "Student");

        Student newStudent = new Student();
        newStudent.setId(student.getId());
        newStudent.setName(student.getName());
        newStudent.setLastname(student.getLastname());

        Student another = new Student(student.getName(), student.getLastname());

        assertNotNull(student);
        assertNotNull(newStudent);
        assertNotNull(another);
    }

    @Test
    public void testStudentCourse() {

        StudentCourse sc = new StudentCourse(1, 2, 3);

        StudentCourse newSC = new StudentCourse(1, 2 );
        newSC.setId(sc.getId());
        newSC.setIdStudent(sc.getIdStudent());
        newSC.setIdCourse(sc.getIdCourse());

        assertNotNull(sc);
        assertNotNull(newSC);
    }

    @Test
    public void testStudentTest() {

        StudentTest st = new StudentTest(1, 2, 3);

        StudentTest newST = new StudentTest();
        newST.setId(st.getId());
        newST.setIdStudent(st.getIdStudent());
        newST.setIdTest(st.getIdTest());
        newST.setResult(st.getResult());

        assertNotNull(st);
        assertNotNull(newST);
    }

    @Test
    public void testTest() {

        com.testcreator.model.Test test = new com.testcreator.model.Test(1, "Test", 2);

        com.testcreator.model.Test newTest = new com.testcreator.model.Test();
        newTest.setId(test.getId());
        newTest.setName(test.getName());
        newTest.setIdInstructor(test.getIdInstructor());

        com.testcreator.model.Test another = new com.testcreator.model.Test(test.getName(), test.getIdInstructor());

        assertNotNull(test);
        assertNotNull(newTest);
        assertNotNull(another);
    }

}
